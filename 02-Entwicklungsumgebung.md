# Entwicklungsumgebung

Als Entwicklungsumgebung setzen wir [Visual Studio Code](https://code.visualstudio.com/) ein.
Das ist "zwar von Microsoft", läuft aber trotzdem stabil ;-)

Visual Studio Code bringt zum einen direkten git-Support mit, andererseits ist es durch Erweiterungen und Plugins stark konfigurierbar, sodass wir direkt aus der Entwickungsumgebung kompilieren, flashen und debuggen können.

## Installation

Die Software lässt sich (für Ubuntu) [als Debian-Package herunterladen](https://code.visualstudio.com/docs/?dv=linux64_deb).
Die Installation funktioniert vermutlich auch mit der grafischen MATE-Paketverwaltung, sicher jedoch auch über die Kommandozeile:

```bash
$ sudo apt install ./code_*_amd64.deb
```

Bei der Installation wird ein neues Repository in die Paketquellen mit aufgenommen, sodass Updates für VSCode über das normale Systemupdate installiert werden.

## Erweiterungen

Einige Erweiterungen sind für die Entwicklung notwendig, andere nur nützlich.
Die folgende Tabelle gibt einen kurzen Überblick:

| Name | erforderlich | Beschreibung |
| ---- |:------------:| ------------ |
| [C/C++][cpptools] | ✓ | C-Unterstützung: Syntax-Highlighting und Überprüfung, Autovervollständigung, Debugging |
| [Cortex-Debug][cortex-debug] | ✓ | Debug-Konfiguration für ARM Cortex Prozessoren (arm-gcc, OpenOCD) |
| [GitLens][gitlens] | – | umfassende Git-Integration |
| [ARM][arm] | – | Syntax-Highlighting für ARM Assembly, falls man mal hineinschauen möchte |
| [LinkerScript][linkerscript] | – | Syntax-Highlighting für LinkerScript-Dateien, falls man mal hineinschauen möchte |

Installationsanweisungen sind jeweils unter dem Link zu finden.

## Konfiguration

Um ein Projekt kompilieren, flashen und debuggen zu können, müssen einige projektspezifische Einstellungen in der Entwicklungsumgebung vorgenommen werden.

Alle (oder zumindest viele dieser) Einstellungen liegen in [JSON-Notation](https://en.wikipedia.org/wiki/JSON) im (versteckten) Ordner `.vscode` und lassen sich daher mit in das git-Repository aufnehmen.

### `c_cpp_properties.json`

Das Plugin [C/C++][cpptools] kann die eingebundenen Bibliotheken scannen und Deklarationen, Funktions-Prototypen, Typen etc. finden und referenzieren.
Damit dies funktioniert müssen beispielsweise der _Include-Path_ sowie Präprozessor-Konstanten so definiert sein, wie sie später beim Kompilieren definiert sind.
Sowohl der Suchpfad für Header-Dateien als auch die Präprozessor-Konstanten sind in der _Makefile_ spezifiziert, von wo sie sich (manuell) in die Plugin-Konfiguration übertragen lassen.
Im Ordner [config/](config/c_cpp_properties.json) liegt eine Beispiels-Konfigurationsdatei für das Messknoten-Projekt.

### `tasks.json`

Aktionen, welche externe Programme benötigen, lassen sich als _Tasks_ in der Konfigurationsdatei `tasks.json` spezifizieren.
Prinzipiell kann alles was in einer Shell ausgeführt werden kann als Task hinterlegt werden, in diesem Fall geht es (bisher) lediglich um das Starten des Build-Prozesses durch Ausführen von [make](01-Toolchain.md#make).
In [config/](config/tasks.json) liegt ebenfalls eine Beispieldatei für das Messknoten-Projekt.

### `launch.json`

Der Debugger soll in VSCode integriert werden, die Konfiguration dazu liegt in der [`launch.json`](config/launch.json).
In dieser Datei sind die verschiedenen Ausführungs-Konfigurationen spezifiziert, in diesem Fall wieder bisher nur eine einzige.
Unter Verwendung des Plugins [Cortex-Debug][cortex-debug] ist eine [Debug-Toolchain mit GDB und OpenOCD](01-Toolchain.md#debugger) deklariert, vor Start der Debug-Session wird zusätzlich noch der [Build-Task](#tasks.json) zum neu Kompilieren ausgeführt.

### `STM32L051x.svd`

Wie [schon erwähnt](01-Toolchain.md#client) lässt sich der Debugger mit einer Beschreibung der Peripherie-Register starten. So ist es möglich, während einer Debug-Session hier einen Blick hineinzuwerfen und im laufenden Betrieb Einstellungen zu ändern oder Eingänge zu überwachen.
In [config/](config/) liegt eine Beschreibungsdatei für Prozessoren der STM32L051x-Reihe, weitere sind [im Internet][svd-files] zu finden.

  [cpptools]: https://marketplace.visualstudio.com/items?itemName=ms-vscode.cpptools
  [cortex-debug]: https://marketplace.visualstudio.com/items?itemName=marus25.cortex-debug
  [gitlens]: https://marketplace.visualstudio.com/items?itemName=eamodio.gitlens
  [arm]: https://marketplace.visualstudio.com/items?itemName=dan-c-underwood.arm
  [linkerscript]: https://marketplace.visualstudio.com/items?itemName=zixuanwang.linkerscript
  [svd-files]: https://github.com/posborne/cmsis-svd/tree/master/data/STMicro