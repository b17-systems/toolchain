# Toolchain

## Compiler und Konsorten

Um Anwendungscode auf dem Prozessor laufen lassen zu können muss man den Code einerseits kompilieren (und linken) können, andererseits muss er irgendwie in den Speicher des Prozessors gelangen.

Wir verwenden hierfür die [GNU ARM-Toolchain](https://developer.arm.com/open-source/gnu-toolchain/gnu-rm/downloads), welche in vielen Betriebssystemen bereits in den Paketquellen vorhanden ist. In [ARM Compiler Toolchain](res/ARM Compiler Toolchain - Developing Software for ARM Processors.pdf) ist einiges zur Hardware und dem System beschrieben, falls hier etwas offen sein sollte.

### Compiler und Linker

Hauptbestandteil dieser Toolchain ist der Compiler `arm-none-eabi-gcc`, welcher _jede einzelne_ Quelldatei `*.c` in eine kompilierte Objektdatei `*.o` umwandelt.
Diese Sammlung an Objektdateien ist alleine noch nicht lauffähig, weil Funktionsaufrufe nach außerhalb der Datei (auch auf Bibliotheksfunktionen) noch nicht verknüpft sind.

An dieser Stelle kommt der Linker ins Spiel: der Linker sucht aus den kompilierten Objektdateien die benötigten Funktionen und Strukturen zusammen und "linkt" diese zu einem lauffähigen Programm.

### make

Das Projekt besteht aus vielen verschiedenen Quelldateien, welche alle kompiliert und anschließend gelinkt werden müssen.
Darüber hinaus benötigt der Compiler verschiedene _flags_ und Konstantendefinitionen, weil einige Quelldateien darauf angewiesen sind.
Dieser ganze Build-Prozess ist über Regeln in der _Makefile_ beschrieben, anhand welcher das Programm _make_ die verschiedenen Schritte ausführen kann.
_make_ ist darüber hinaus in der Lage, Änderungen an Quelldateien festzustellen, weshalb häufig nur die geänderten Komponenten neu kompiliert werden müssen.

### Standardbibliothek

Die Standardbibliothek [newlib](https://en.wikipedia.org/wiki/Newlib) ist auf den Einsatz in eingebetteten Systemen optimiert.
Sie besteht aus einer Implementierung der _C Standard Library_ [`libc`](https://www.sourceware.org/newlib/libc.html) sowie einer der Mathebibliothek [`libm`](https://www.sourceware.org/newlib/libm.html).

## Programmer

Irgendwie muss das kompilierte Programm jetzt in den Speicher des Mikrocontrollers geladen werden. Da der Mikrocontroller nur einen internen Speicher ohne nach außen geführten Bus besitzt, ist dieser nur von innerhalb des Mikrocontrollers schreib- und lesbar.
Möchte man doch von außen direkt auf den Speicher zugreifen lässt sich dies einerseits über die Debug-Schnittstelle, andererseits über einen Bootloader bewerkstelligen.

### Bootloader

Der Prozessor hat einen Bootloader im ROM, welcher in der Lage ist, Daten **direkt nach einem Reset** auf verschiedenen peripheren Bussen entgegen zu nehmen, auf Integrität zu prüfen und anschließend in den Flash-Speicher zu laden.
Kommen in der Zeitspanne direkt nach dem Neustart keine Daten, fährt der Prozessor mit dem Bootprozess fort und verlässt den Bootloader.

Die verschiedenen Boot-Modi sind in [AN2606](res/AN2606 - STM32 microcontroller system memory boot mode.pdf) zusammengefasst, [AN3155](res/AN3155 - USART protocol used in the STM32 bootloader.pdf) beschreibt detailliert das in diesem Bootloader verwendete U(S)ART-Protokoll.
Wenn wir doch irgendwann über den Bus ganze Firmware-Updates machen können wollen, könnte das für uns interessant werden.

### Debug-Port

Neben dem von ST installierten Bootloader unterstützt die Architektur selbst direkt einige Debug-Protokolle, mit welchen nicht nur die Ausführung pausiert, sondern auch der Speicher geschrieben und gelesen werden kann.
Man interagiert somit zwar nicht direkt mit dem Speicher, kommt aber dran ohne dass die CPU selbst mit im Spiel ist.
Mit einem entsprechenden Interface und der nötigen Software ist der Rechner in der Lage, diese Schnittstelle zu bedienen und am Prozessor vorbei in den Speicher zu schreiben bzw. ihn zu lesen.

Eine solche Hardware ist der ST-Link v2.1, in diesem Fall der Abbrech-Teil eines ausrangierten [Nucleo](https://www.st.com/en/evaluation-tools/nucleo-f446re.html). Er besteht (primär) aus einer USB-Schnittstelle, einem Prozessor und Anschlüssen zum zu programmierenden Prozessor.
Auf dem Prozessor des Programmers läuft eine Software von ST und auf einem Linux-Rechner sind bereits die nötigen Treiber installiert _um mit dem Programmer sprechen zu können_.
Um jetzt eine Anwendung in den Speicher laden zu können ist noch ein Programm notwendig, welches das ST-Link-Protokoll beherrscht und eine Datei vom Rechner übertragen kann. Hierfür gibt es einfach zu bedienende (und einfach zu konfigurierende) eigenständige Programme, da wir aber später ohnehin noch einen Debugger benötigen können wir es direkt mit dem Debug-Server machen.

## Debugger

Anders als beim Debuggen eines "herkömmlichen" Programms laufen Anwendung und Debugger in unserem Fall nicht auf der gleichen Hardware.
Aus diesem Grund ist die bereits oben erwähnte Debug-Schnittstelle des Mikroprozessors notwendig um den Code schrittweise ausführen zu können oder im Betrieb Speicherinhalt auszulesen und zu schreiben.
Das Setup besteht aus zwei Komponenten: einem _Debug Server_ welcher den Kommunikationskanal in den Mikroprozessor bereit stellt und einem _Debugger Client_ welcher die tatsächliche Ablaufsteuerung übernimmt.

### Server

Als Debug-Server setzen wir den on-chip debugger [OpenOCD](http://openocd.org/). Dieser wird als Daemon gestartet und bekommt Informationen sowohl über das Interface (ST-Link) als auch über das Zielsystem (STM32L0-Serie). Über eine TCP-Verbindung (telnet) lässt sich dann eine Debug-Session initialisieren, in welcher dann die Ausführung gestartet, pausiert oder gestoppt werden kann, (Hardware-)Breakpoints gesetzt oder gelöscht werden können, der Speicher geschrieben und gelesen werden kann und allgemein wer weiß sonst was mit dem Prozessor angestellt werden kann. In der [User's Guide](res/OpenOCD User's Guide.pdf) sind die Befehle und Möglichkeiten aufgelistet.

OpenOCD verwenden wir um über den Befehl `flash write-image [program.elf]` das kompilierte Programm in den Speicher des Mikrocontrollers zu laden.

### Client

Mit OpenOCD lässt sich zwar in den Speicher gucken, jedoch sind diese Informationen meistens ohne Kontext unbrauchbar.
Möchte man den Speicherinhalt bestimmten Variablenwerten, den _Program Counter_ Zeilen in Quellcode-Dateien und den _Call Stack_ einer Reihe von Funktionsaufrufen zuordnen können, benötigt man ein Frontend, welches den Debug-Server steuert.

Wir verwenden hierfür den GNU-Debugger GDB, konfiguriert für die ARM-Architektur.
Dieser bekommt "seine eigene Version" des kompilierten Programms sowie einen Haufen Dateien, mit welchen der Zusammenhang zwischen Quelldateien und Speicher (Flash, RAM, EEPROM) des Mikrocontrollers beschrieben wird.
Als Bonus ist eine Datei hinterlegt, welche die Peripherie-Register des Prozessors beschreibt. So lassen sich nicht nur verwendete Variablen mit Namen ansprechen, sondern ebenfalls die verschiedenen Register für Konfiguration von und Interaktion mit Peripheriekomponenten des Prozessors (GPIOs, ADC, verschiedene Busse, Timer, Systemkonfiguration etc.) manipulieren.

Dieser Debugger lässt sich in unsere Entwicklungsumgebung integrieren, sodass er sich aus einer grafischen Oberfläche heraus steuern lässt. Breakpoints, Call Stack, Variablen und _Watch Expressions_ funktionieren wie beim Debuggen von Rechner-Anwendungen :-)

## Installation (Ubuntu)

Zunächst müssen die verschiedenen Pakete installiert werden:

```bash
$ sudo apt-add-repository ppa:team-gcc-arm-embedded/ppa && sudo apt update
$ sudo apt install make gcc-arm-embedded openocd
```

Um als normaler Benutzer auf die serielle Schnittstelle zugreifen zu dürfen, ist es am einfachsten dies global zu erlauben.
Serielle Schnittstellen gehören dem Benutzer _root_, normale Benutzer haben hierauf standardmäßig keinen Schreibzugriff.
Um dies trotzdem zu ermöglichen, lassen sich gerätespezifische Ausnahmen definieren.
Im Ordner [config/udev](config/udev) liegen [udev](https://wiki.archlinux.org/index.php/udev)-Dateien für verschiedene Geräte der ST-Link Familie.
Diese müssen nach `/etc/udev/rules.d/` kopiert und der Dienst neu geladen werden, dann darf auch ein normaler Benutzer auf diese Geräte zugreifen.

```bash
$ sudo cp config/udev/*.rules /etc/udev/rules.d/
$ sudo udevadm control --reload-rules && sudo udevadm trigger
```

## Versionsverwaltung

Zur Versionsverwaltung setzen wir [Git](https://git-scm.com/) ein, gehostet ist das Projekt auf [GitLab](https://about.gitlab.com/).

Auf eine detaillierte Einführung in Git soll an dieser Stelle verzichtet werden, davon gibt es im Netz genug. Atlassian hat [eine](https://www.atlassian.com/git/tutorials/what-is-version-control) [Reihe](https://www.atlassian.com/git/tutorials/setting-up-a-repository) [von](https://www.atlassian.com/git/tutorials/syncing) [Tutorials](https://www.atlassian.com/git/tutorials/advanced-overview) über Git und Versionsverwaltung im Allgemeinen, sehr zu empfehlen ist auch das [Git Book](https://git-scm.com/book/en/v2) welches in Teilen sogar [auf deutsch](https://git-scm.com/book/de/v2) vorliegt.

Ubuntu kommt ohne vorinstalliertes git, daher ist dies schnellstmöglich nachzuholen:

```bash
$ sudo apt install git
```

Git hat Schwierigkeiten mit der Versionierung großer Binärdateien, daher wird hierfür die Erweiterung [_Git Large File Storage (LFS)_](https://git-lfs.github.com/) eingesetzt.
Nachdem diese installiert ist muss sie nicht weiter beachtet werden, alles relevante läuft im Hintergrund ab.

```bash
$ sudo apt install curl  # auch ein de-facto-Standard
$ curl -s https://packagecloud.io/install/repositories/github/git-lfs/script.deb.sh | sudo bash
$ sudo apt install git-lfs
$ git lfs install
```