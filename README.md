# Toolchain

Dieses Repository beinhaltet Dokumentation über die Toolchain und Resourcen für die STM32-Entwicklung unter Linux.

Im Abschnitt [**Toolchain**](01-Toolchain.md) sind Informationen über die Toolchain selbst enthalten, also den Weg vom (fertig konfigurierten) Softwareprojekt bis zur Debug-Session auf der Zielhardware.
Mit der dort beschriebenen Software lässt sich das Messknoten-Projekt (haustechnik/node>) klonen und kompilieren.

Der Abschnitt [**Entwicklungsumgebung**](02-Entwicklungsumgebung.md) wird auf die Entwicklung und deren Installation sowie Konfiguration eingegangen.
Nach erfolgter Konfiguration lässt sich die Messknoten-Software sowohl mit Syntax-Highlighting und kontextsensitiver Autovervollständigung bearbeiten als auch flashen und auf dem Mikrocontroller debuggen.

Im Dokument [**Plattform**](03-Plattform.md) ist die eingesetzte Hardware-Plattform näher beschrieben. Es wird sowohl auf die Hardware und deren genaue Prozessorarchitektur als auch auf die Software zur Peripheriekonfiguration und Codegenerierung eingegangen. Darüber hinaus sind die verwendeten Low-Level Bibliotheken und in Grundzügen das auf der Zielhardware eingesetzte Echtzeitbetriebssystem erläutert.
Mit den Informationen aus diesem Abschnitt lässt sich das Messknoten-Projekt softwareseitig weiterentwicklen.

Den (vorerst) letzten Teil macht der Abschnitt [**Peripherie**](04-Peripherie.md), in welchem einige der für das betrachtete Projekt relevanten System- und Peripheriekomponenten des Mikrocontrollers kurz angesprochen und umrissen werden.
Dieser Überblick sollte den ersten Einblick in die Eigenschaften und Möglichkeiten der Hardware vervollständigen und eine Basis für die (weitere) Softwareentwicklung bilden.