# Plattform

Auf der Messknoten-Platine ist ein Prozessor der STM32-Reihe von STMicroelectronics verbaut.
An dieser Stelle soll etwas auf die gesamte Plattform bezüglich Hard- und Software eingegangen werden, die peripheren Komponenten des Prozessors sind [im nächsten Kapitel](04-Peripherie.md) detaillierter erläutert.

## Hardware und Architektur

Der verbaute Prozessor ist ein [STM32L051R8][] aus der _Ultra-Low-Power_-Reihe [STM32L0][] von STMicroelectronics.
Er verfügt über einen _ARM Cortex-M0+ 32-Bit RISC_ Kern, 64 KB Flash-Speicher, 2 KB EEPROM und satte 8 KB RAM.
Als Prozessor der _Access Line_ [STM32L0x1][] liegt der Fokus auf einer großen Anzahl an Peripheriekomponenten sowie niedrigem Energieverbrauch.

Da die Architektur des Prozessors zur ARM-Familie gehört, lässt sich die generische [ARM-Toolchain][toolchain] verwenden, welche für viele Linux-Betriebssysteme sehr einfach erhältlich ist.
Details über die Cortex-M0+ Architektur sind zwar für die Programmierung nicht zwingend erforderlich, können aber ganz interessant sein. Näheres ist der [Generic User Guide][cortex-ug] sowie dem [Technical Reference Manual][cortex-rm] zu entnehmen.

  [STM32L0]: https://www.st.com/en/microcontrollers/stm32l0-series.html
  [STM32L0x1]: https://www.st.com/en/microcontrollers/stm32l0x1.html
  [STM32L051R8]: https://www.st.com/content/st_com/en/products/microcontrollers/stm32-32-bit-arm-cortex-mcus/stm32-ultra-low-power-mcus/stm32l0-series/stm32l0x1/stm32l051r8.html
  [toolchain]: 01-Toolchain.md
  [cortex-rm]: res/L051/Cortex-M0+%20-%20Technical%20Reference%20Manual.pdf
  [cortex-ug]: res/L051/Cortex-M0+%20Devices%20-%20Generic%20User%20Guide.pdf

## STM32CubeMX

Der Prozessor verfügt über viele verschiedene Peripheriekomponenten, welche wiederum häufig eine große Menge an konfigurierbaren Einstellungen besitzen.
Zwar wäre es möglich sich anhand des [Reference Manuals][RM0377] die nötigen Werte für die Konfigurationsregister herauszusuchen, jedoch ist dies langwierig, umständlich und fehleranfällig.
Aus diesem Grund bietet ST mit dem Programm [STM32CubeMX][] einen grafischen Konfigurator an, mit welchem sich die Hardwarekomponenten konfigurieren und Initialisierungscode generieren lassen.
[Das Manual][UM1718] schildert relativ detailliert wie mit dem Programm umzugehen ist, vieles ist aber auch selbsterklärend.

  [RM0377]: res/L051/RM0377%20-%20Ultra-low-power%20STM32L0x1%20advanced%20ARM-based%2032-bit%20MCUs.pdf
  [STM32CubeMX]: https://www.st.com/en/development-tools/stm32cubemx.html
  [UM1718]: res/UM1718%20-%20STM32CubeMX%20for%20STM32%20configuration%20and%20initialization%20C%20code%20generation.pdf

### Installation

Von der [Website][STM32CubeMX] lässt sich die Software nach vorheriger Anmeldung als zip-Archiv herunterladen, die Installation unter Linux ist jedoch etwas tückisch. Aus diesem Grund steht [hier](https://f001.backblazeb2.com/file/antares/STM32CubeMX.tar.gz) ein tar-Archiv zum Download bereit, welches nur noch in das Opt-Verzeichnis `/opt/` entpackt werden muss:

```bash
$ curl https://f001.backblazeb2.com/file/antares/STM32CubeMX.tar.gz | sudo tar xzC /opt/
```

Das Programm ist eine 32-Bit Anwendung und benötigt neben einer aktuellen Java-Version auch eine bestimmte 32-bit Systembibliothek:

```bash
$ sudo apt install openjdk-8-jre libc6-i386
```

Schlussendlich kann die ausführbare Datei noch als Programm beim System angemeldet werden, damit sie auch z.B. im Startmenü auftaucht. Hierzu muss die [STM32CubeMX.desktop](config/STM32CubeMX.desktop)-Datei mit `desktop-file-install` installiert werden.
Achtung: diese Datei verwendet absolute Pfade und funktioniert nur, wenn CubeMX nach `/opt/STM32CubeMX/` installiert wurde!

```bash
$ sudo desktop-file-install STM32CubeMX.desktop
```

## Hardware Abstraction Layer (HAL)

Die Interaktion mit Peripheriekomponenten läuft generell auch über deren Konfigurations- und Datenregister. Somit müsste man um z.B. eine Analog-Digital-Wandlung durchzuführen, eine Nachricht über den Bus zu schicken oder einfach nur eine Leuchtdiode blinken zu lassen doch wieder das [Manual][UM1718] lesen und mit Bits in Registern spielen.

Da das wie bereits erwähnt umständlich ist, bietet ST Interface-Bibliotheken an, mit deren Hilfe die Peripheriekomponenten auf einem höheren Layer angesprochen werden können.
Den Hauptteil dieser Bibliotheken bilden die _Hardware Abstraction Layer (HAL)_ Treiber. Die HAL-API ist zu einem großen Teil plattformunabhängig, sodass sich Code ohne (allzu) große Schwierigkeiten von einem Prozessor auf einen anderen portieren lässt. Für einige Komponenten existieren darüber hinaus noch _Low Layer (LL)_ Treiber, welche einen etwas effizienteren Zugriff auf diese Systeme liefert.
Beide Bibliotheken und deren APIs sind [im Handbuch][UM1749] beschrieben, vieles lässt sich jedoch auch durch die Autovervollständigung in VSCode und die Dokumentation in den Header-Dateien zusammenreimen.

  [UM1749]: res/L051/UM1749%20-%20Description%20of%20STM32L0%20HAL%20and%20Low%20Layer%20drivers.pdf

## FreeRTOS

Ohne ein Betriebssystem ("bare-metal") ist der Prozessor zu Multitasking (natürlich) nicht in der Lage. Da eine Anwendung in den meisten Fällen mehrere Aufgaben parallel ausführen soll (z.B. Reagieren auf Ereignisse auf einem Bus und zeitgleich zyklisches Einlesen von Messwerten aus der Peripherie), ist es häufig sinnvoll ein Betriebssystem einzusetzen. Da jedoch die Ressourcen auf einem Mikrocontroller häufig sehr knapp sind (Programmspeicher, RAM, Laufzeit), muss das Betriebssystem entsprechend schlank und für den Einsatzzweck ausgelegt sein.

[FreeRTOS][] ist ein solches _Real Time Operating System (RTOS)_, welches neben einem Event Loop zum Multitasking auch Konstrukte wie _Queues_, _Semaphores_ und (Software-) Timer bietet. FreeRTOS ist neben I/O-Konfiguration ebenfalls in [CubeMX](#STM32CubeMX) enthalten, sodass sich Elemente wie Tasks und Timer direkt grafisch konfigurieren lassen.
Es gibt ein [FreeRTOS Reference Manual][FreeRTOS-manual] sowie eine [Hands-On Tutorial Guide][FreeRTOS-guide], in welchen Funktionsweise und API des Systems näher erläutert werden. Zusätzlich kann sich für einen schnellen Überblick noch ein Blick [auf die Website][FreeRTOS] lohnen.

  [FreeRTOS]: https://www.freertos.org/FreeRTOS-quick-start-guide.html
  [FreeRTOS-manual]: res/FreeRTOS%20Reference%20Manual.pdf
  [FreeRTOS-guide]: res/Mastering%20the%20FreeRTOS%20Real%20Time%20Kernel%20-%20A%20Hands-On%20Tutorial%20Guide.pdf